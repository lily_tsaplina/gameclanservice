package ru.mail.course;

import io.vertx.core.*;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.AsyncMap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.Duration;
import java.time.temporal.TemporalUnit;
import java.util.Map;

import static java.time.temporal.ChronoUnit.SECONDS;

public final class Admin extends AbstractVerticle {
    @NotNull
    private String adminName;
    @Nullable
    private String clan;

    private final int USERS_LIMIT = 5;
    private final int MODERATORS_LIMIT = 1;

    public static void main(@NotNull String[] args) {
        Vertx.clusteredVertx(new VertxOptions(), res -> {
            if (res.succeeded()) {
                final JsonObject config = new JsonObject().put("name", "admin#");
                final DeploymentOptions options = new DeploymentOptions()/*.setWorker(true)*/.setConfig(config);
                res.result().deployVerticle(new Admin(), options);
            }
        });
    }

    @Override
    public void start(@NotNull Promise<Void> startPromise) {
        vertx.sharedData().getCounter("admins", counter -> {
            if (counter.succeeded()) {
                counter.result().incrementAndGet(counterRes -> {
                    adminName = config().getString("name") + counterRes.result();
                    System.out.println(adminName + " started to work");

                    vertx.setPeriodic(Duration.of(5, SECONDS).toMillis(), timer -> {
                        //найти неактивный клан, стать его админом
                        vertx.sharedData().<String, JsonObject>getAsyncMap("clans", res -> {
                            if (res.succeeded()) {
                                AsyncMap<String, JsonObject> map = res.result();
                                map.entries(entries -> {
                                    entries.result().entrySet().stream()
                                            .filter(e -> e.getValue().getString("admin").isEmpty())
                                            .findFirst()
                                            .ifPresent(entryVal -> {
                                                String clan = entryVal.getKey();
                                                JsonObject clanInfo = entryVal.getValue();
                                                map.put(
                                                        entryVal.getKey(),
                                                        clanInfo
                                                                .put("admin", adminName)
                                                                .put("usersLimit", USERS_LIMIT)
                                                                .put("moderatorsLimit", MODERATORS_LIMIT),
                                                        event -> {
                                                            vertx.cancelTimer(timer);
                                                            this.clan = entryVal.getKey();
                                                            System.out.println(adminName + " sent msg to " + clan + ".updateInfo");
                                                            vertx.eventBus().send(clan + ".updateInfo", "");
                                                            System.out.println(adminName + " became admin of " + clan);
                                                            if (clanInfo.getInteger("usersCount") > USERS_LIMIT
                                                                    || clanInfo.getInteger("moderatorsCount") > MODERATORS_LIMIT) {
                                                                System.out.println(adminName + " says all members of " + clan + " to left and join again the clan");
                                                                vertx.eventBus().publish(clan + ".needToRejoin", "");
                                                            }
                                                        });
                                            });
                                });
                            }
                        });
                    });
                });
            }

            startPromise.complete();
        });

    }

    @Override
    public void stop(@NotNull Promise<Void> stopPromise) {
        if (clan != null) {
            vertx.sharedData().<String, JsonObject>getAsyncMap("clans", res -> {
                if (res.succeeded()) {
                    AsyncMap<String, JsonObject> map = res.result();
                    map.get("clan", clanObj -> {
                        map.put(
                                clan,
                                clanObj.result().put("admin", ""),
                                event -> System.out.println(adminName + " left " + clan)
                        );
                        vertx.eventBus().send(clan + ".updateInfo", "");
                    });
                }
            });
        }
    }
}
