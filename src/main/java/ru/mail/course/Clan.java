package ru.mail.course;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.LocalMap;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public final class Clan extends AbstractVerticle {
    private int usersLimit = 10;
    private int moderatorsLimit = 5;
    @NotNull
    private String admin = "";
    @NotNull
    private String clanName;
    @NotNull
    private LocalMap<String, String> users;
    @NotNull
    private LocalMap<String, String> moderators;

    public static void main(@NotNull String[] args) {
        Vertx.clusteredVertx(new VertxOptions(), res -> {
            if (res.succeeded()) {
                res.result().deployVerticle(new Clan());
            }
        });
    }

    @Override
    public void start(@NotNull Promise<Void> startPromise) {
        users = vertx.sharedData().getLocalMap("users");
        moderators = vertx.sharedData().getLocalMap("moderators");
        vertx.sharedData().getCounter("clans", counter -> {
            if (counter.succeeded()) {
                counter.result().incrementAndGet(n -> {
                    clanName = "clan#" + n.result();
                    System.out.println(clanName + "started");
                    vertx.sharedData().<String, JsonObject>getAsyncMap("clans", res -> {
                        if (res.succeeded()) {
                            res.result().put(
                                    clanName,
                                    new JsonObject()
                                            .put("admin", "")
                                            .put("usersLimit", usersLimit)
                                            .put("moderatorsLimit", moderatorsLimit)
                                            .put("usersCount", 0)
                                            .put("moderatorsCount", 0),
                                    event -> {
                                        System.out.println(clanName + " added itself to shared map");
                                    });
                        }
                    });

                    vertx.eventBus().consumer(clanName + ".updateInfo", message -> {
                        updateInfo();
                    });

                    vertx.eventBus().consumer(clanName + ".members", event -> {
                        List<String> members = new ArrayList<>(users.keySet());
                        members.addAll(moderators.keySet());
                        members.add(admin);
                        event.reply(new JsonArray(members));
                    });

                    vertx.eventBus().consumer(clanName + ".placeForUser", event -> {
                        boolean thereIsPlaceForUser = users.size() < usersLimit;
                        event.reply(thereIsPlaceForUser);
                    });

                    vertx.eventBus().<String>consumer(clanName + ".leave", message -> {
                        String member = message.body();
                        users.remove(member);
                        moderators.remove(member);
                        updateInfo();
                    });

                    vertx.eventBus().<String>consumer(clanName + ".acceptUser", event -> {
                        String user = event.body();
                        users.put(user, user);
                        vertx.sharedData().<String, JsonObject>getAsyncMap("clans", res -> {
                            if (res.succeeded()) {
                                res.result().get(clanName, getEvent -> {
                                    res.result().put(
                                            clanName,
                                            getEvent.result().put("usersCount", users.size()),
                                            putEvent -> System.out.println(user + " joined " + clanName));
                                });
                            }
                        });
                    });

                    vertx.eventBus().<String>consumer(clanName + ".addModeratorIfPossible", event -> {
                        if (moderators.size() < moderatorsLimit) {
                            String moderator = event.body();
                            moderators.put(moderator, moderator);

                            vertx.sharedData().<String, JsonObject>getAsyncMap("clans", res -> {
                                if (res.succeeded()) {
                                    res.result().get(clanName, getEvent -> {
                                        res.result().put(
                                                clanName,
                                                getEvent.result().put("moderatorsCount", moderators.size()),
                                                putEvent -> System.out.println(moderator + " joined " + clanName));
                                        event.reply(true);
                                    });
                                } else {
                                    event.reply(false);
                                }
                            });
                        } else {
                            event.reply(false);
                        }
                    });
                });
            }
        });
    }

    private void updateInfo() {
        vertx.sharedData().getAsyncMap("clans", clans -> {
            if (clans.succeeded()) {
                clans.result().<JsonObject>get(clanName, clanInfo -> {
                    if (clanInfo.succeeded()) {
                        JsonObject json = new JsonObject(clanInfo.result().toString());
                        admin = json.getString("admin");
                        usersLimit = json.getInteger("usersLimit");
                        moderatorsLimit = json.getInteger("moderatorsLimit");
                    }
                });
            }
        });
    }
}
