package ru.mail.course;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.AsyncMap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.Duration;
import java.util.concurrent.ThreadLocalRandom;

import static java.time.temporal.ChronoUnit.SECONDS;

public final class User extends AbstractVerticle {
    @Nullable
    private String name;

    @Nullable
    private String clan;

    private final @NotNull Duration resendJoinRequestPeriod = Duration.ofSeconds(5);

    public static void main(@NotNull String[] args) {
        Vertx.clusteredVertx(
                new VertxOptions(),
                vertx -> vertx.result().deployVerticle(new User())
        );
    }

    @Override
    public void start(@NotNull Promise<Void> startPromise) {
        vertx.sharedData().getCounter("users", counter -> {
            if (counter.succeeded()) {
                counter.result().incrementAndGet(n -> {
                    name = "user#" + n.toString();
                    System.out.println(name + "started to work");
                    vertx.sharedData().<String, JsonObject>getAsyncMap("clans", res -> {
                        if (res.succeeded()) {
                            AsyncMap<String, JsonObject> map = res.result();
                            map.entries(entries -> {
                                entries.result().entrySet().stream()
                                        .filter(entry -> !entry.getValue().getString("admin").isEmpty())
                                        .findFirst()
                                        .ifPresent(entry -> {
                                            joinTheClan(entry.getKey());
                                        });
                            });
                            startPromise.complete();
                        }
                    });
                });
            }
        });
    }

    private void joinTheClan(@NotNull String clan) {
        System.out.println(name + " tries to join to the " + clan);
        vertx.eventBus().<Boolean>request(
                clan + ".join",
                name,
                response -> {
                    if (response.succeeded()) {
                        if (response.result().body()) {
                            System.out.println(name + " knows that his join request was accepted");
                            this.clan = clan;
                            vertx.setPeriodic(Duration.of(5, SECONDS).toMillis(), timer -> {
                                //get clan members and send to one of them a message
                                vertx.eventBus().<JsonArray>request(clan + ".members","", message -> {
                                    if (message.succeeded()) {
                                        JsonArray members = message.result().body();
                                        System.out.println(name + " sends \"Hello\" to random member");
                                        vertx.eventBus().send((String)members.getValue(ThreadLocalRandom.current().nextInt(members.size())),
                                                "Hello");
                                    }
                                });
                            });
                        } else {
                            vertx.eventBus().consumer(clan + ".needToRejoin", message -> {
                                System.out.println(name + " wants to left " + clan);
                                vertx.eventBus().request(clan + ".leave", name, reply -> {
                                    if (reply.succeeded()) {
                                        joinTheClan(clan);
                                    }
                                });
                            });
                        }
                    } else {
                        //с некоторой вероятностью послать повторно запрос на вступление (или сделать это позднее)
                        joinTheClan(clan);
                    }
                }
        );
    }

    @Override
    public void stop(@NotNull Promise<Void> stopPromise) {
        vertx.eventBus().send(clan + ".leave", name);
        stopPromise.complete();
    }
}
