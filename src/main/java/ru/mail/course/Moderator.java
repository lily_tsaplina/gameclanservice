package ru.mail.course;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.AsyncMap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.SECONDS;

public final class Moderator extends AbstractVerticle {
    @Nullable
    private String moderatorName;
    @Nullable
    private String clan;

    public static void main(@NotNull String[] args) {
        Vertx.clusteredVertx(
                new VertxOptions(),
                vertx -> vertx.result().deployVerticle(new Moderator())
        );
    }

    @Override
    public void start(@NotNull Promise<Void> startPromise) {
        vertx.sharedData().getCounter("moderators", counter -> {
            if (counter.succeeded()) {
                counter.result().incrementAndGet(counterRes -> {
                    moderatorName = "moderator#" + counterRes.result();
                    System.out.println(moderatorName + " started");

                    //найти активный клан, стать его модератором
                    vertx.sharedData().<String, JsonObject>getAsyncMap("clans", res -> {
                        if (res.succeeded()) {
                            AsyncMap<String, JsonObject> map = res.result();
                            map.entries(entries -> {
                                List<Map.Entry<String, JsonObject>> entryList = entries.result().entrySet().stream()
                                        .filter(e -> !e.getValue().getString("admin").isEmpty())
                                        .collect(Collectors.toList());
                                for (Map.Entry<String, JsonObject> entry : entryList) {
                                    if (clan != null){
                                        break;
                                    }
                                    String clanName = entry.getKey();
                                    vertx.setPeriodic(Duration.of(5, SECONDS).toMillis(), timer -> {
                                        System.out.println(moderatorName + " tries to join " + clanName);
                                        vertx.eventBus().<Boolean>request(
                                                clanName + ".addModeratorIfPossible",
                                                moderatorName,
                                                reply -> {
                                                    if (reply.succeeded()) {
                                                        Object body = reply.result().body();
                                                        if ((boolean) body) {
                                                            vertx.eventBus().<String>consumer(clanName + ".join", event -> {
                                                                final String userName = event.body();
                                                                System.out.println(userName + " wants to join the clan " + clanName);
                                                                vertx.cancelTimer(timer);
                                                                //одобряет заявку в состоянии ожидания, если лимит на количество участников не превышен
                                                                vertx.eventBus().<Boolean>request(clan + ".placeForUser", "", placeForUserReply -> {
                                                                    if (placeForUserReply.succeeded()) {
                                                                        Object replyBody = placeForUserReply.result().body();
                                                                        boolean thereIsPlaceForUser = (boolean) replyBody;
                                                                        if (thereIsPlaceForUser) {
                                                                            vertx.eventBus().request(clanName + ".acceptUser", userName, accept -> {
                                                                                System.out.println("join request of " + userName + " was accepted");
                                                                                event.reply(true);
                                                                            });
                                                                        } else {
                                                                            event.reply(false);
                                                                        }
                                                                    }
                                                                });
                                                            });
                                                            clan = clanName;
                                                        }
                                                    }
                                        });
                                    });
                                }
                            });
                        }
                    });
                });
            }
        });

        startPromise.complete();
    }

    @Override
    public void stop(@NotNull Promise<Void> stopPromise) {
        vertx.eventBus().send(clan + ".leave", moderatorName);
        stopPromise.complete();
    }
}
